package org.training.producer.configuration;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;
import org.training.producer.dto.GlobalConstants;

@Configuration
public class TopicConfiguration {
 
	@Bean
	NewTopic createTopic() {
		return TopicBuilder.name(GlobalConstants.TOPIC)
				.partitions(2).build();
	}
}
