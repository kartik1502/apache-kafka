package org.training.producer.configuration;

import java.util.Objects;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serializer;
import org.training.producer.dto.Customer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class KafkaSerializer implements Serializer<Customer>{

	private final ObjectMapper mapper = new ObjectMapper();
	
	@Override
	public byte[] serialize(String topic, Customer data) {
		
		
		log.info("serializing the object -> byte stream");
		try {
			if(Objects.isNull(data)) {
				log.error("Null received at the serialization...");
				return null;
			}
			return mapper.writeValueAsBytes(data);
		} catch (JsonProcessingException e) {
			throw new SerializationException("Error occurred while serializing the object -> byte stream");
		}
	}

	
}
