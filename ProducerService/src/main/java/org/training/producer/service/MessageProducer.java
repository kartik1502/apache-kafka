package org.training.producer.service;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Service;
import org.training.producer.dto.GlobalConstants;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MessageProducer {
	
	public Future<RecordMetadata> produce(final String message) {
		
		Properties properties = new Properties();
		properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		properties.put(ProducerConfig.ACKS_CONFIG, "all");
		
		try (KafkaProducer<String, String> producer = new KafkaProducer<>(properties)) {
			final String[] parts = message.split("-");
			final String key, value;
			if(parts.length > 0) {
				key = parts[0];
				value = parts[1];
			} else {
				key = null;
				value = parts[0];
			}
			
			final ProducerRecord<String, String> producerRecord = new ProducerRecord<String, String>(GlobalConstants.TOPIC, key, value);
			return producer.send(producerRecord);
		}
	}
	
	public void printMetadata(final List<Future<RecordMetadata>> metadata, final String filePath) {
		
		log.info("Offset and partition of each data: ");
		metadata.forEach(data -> {
			try {
				final RecordMetadata recordMetadata = data.get();
				log.info("Record: {} written to partition: {} and offset: {}", recordMetadata.partition(), recordMetadata.offset());
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		});
	}
 }
