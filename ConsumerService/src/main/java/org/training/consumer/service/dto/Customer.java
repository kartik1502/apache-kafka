package org.training.consumer.service.dto;

import lombok.Data;

@Data
public class Customer {

	private int id;
	
	private String name;
	
	private String emailId;
}
