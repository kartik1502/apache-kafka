package org.training.consumer.service.utils.implementation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.training.consumer.service.utils.ConsumerRecordHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ConsumerRecordFileWritingService implements ConsumerRecordHandler<String, String> {

	private final Path path;
	
	public ConsumerRecordFileWritingService(final Path path) {
		this.path = path;
	}
	

	@Override
	public void process(ConsumerRecords<String, String> consumerRecords) {
		
		final List<String> values = new ArrayList<>();
		consumerRecords.forEach(record -> values.add(record.value()));
		if(!values.isEmpty()) {
			try {
				Files.write(path, values, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.APPEND);
			} catch (IOException e) {
				log.error("Error in writing the file");
				e.printStackTrace();
			}
		}
	}

	

}