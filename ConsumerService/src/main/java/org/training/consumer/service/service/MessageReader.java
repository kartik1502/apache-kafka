package org.training.consumer.service.service;

import java.nio.file.Paths;
import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.streams.Topology.AutoOffsetReset;
import org.springframework.stereotype.Service;
import org.training.consumer.service.dto.GlobalConstants;
import org.training.consumer.service.utils.ConsumerRecordHandler;
import org.training.consumer.service.utils.implementation.ConsumerRecordFileWritingService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MessageReader {
	
	final ConsumerRecordHandler<String, String> consumerRecordHandler = new ConsumerRecordFileWritingService(Paths.get("output.txt"));
	
	private volatile boolean consume = true;
	
	public void runConsumer() {
		
		Properties properties = new Properties();
		properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
		properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		properties.put(ConsumerConfig.GROUP_ID_CONFIG, "consumer-application-1");
		
		try (Consumer<String, String> consumer = new KafkaConsumer<>(properties)) {
			consumer.subscribe(Collections.singletonList(GlobalConstants.TOPIC));
			while(consume) {
				final ConsumerRecords<String, String> records = consumer.poll(Duration.ofSeconds(1));
				consumerRecordHandler.process(records);
			}
		}
	}
}
